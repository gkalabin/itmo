__contains() {
  local item target=$1
  shift
  for item; do
    if [ "$item" == "$target" ]
    then
      return 0
    fi
  done
  return 1
}

__array_idx() {
  __contains $@ || echo "you are doing it wrong"
  local item idx=0 target=$1
  shift
  for item; do
    if [ "$item" == "$target" ]
    then
      return $idx
    else
      idx=$((idx+1))
    fi
  done
}

_set_checked() {
  local cur prev opts wcnt
  local BASE_DIR=~/itmo/students/
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}" # current word being typed
  prev="${COMP_WORDS[COMP_CWORD-1]}" # previous word being typed
  opts="--group --student --commit" # out list of options to complete
  wcnt=${#COMP_WORDS[@]} # count of words
  
  # in case of unfinished key
  if [[ ${cur} == -* ]] 
  then
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0
  fi

  local MY_GROUPS=(2742 2743)
  if [[ ${prev} == "--group" ]]
  then
    __contains $cur "${MY_GROUPS[@]}"
    if [[ $? -ne 0 ]]
    then
      COMPREPLY=("${MY_GROUPS[@]}")
    else
      COMPREPLY=$cur
    fi
    return 0
  fi

  if [[ ${prev} == "--student" ]]
  then
    local myGroup=2743
    __contains "--group" "${COMP_WORDS[@]}"
    if [[ $? -eq 0 ]]
    then
      __array_idx "--group" "${COMP_WORDS[@]}"
      local grpIdx=$?
      grpIdx=$((grpIdx+1))
      myGroup=${COMP_WORDS[$grpIdx]}
      __contains $myGroup "${MY_GROUPS[@]}"
      if [[ $? -ne 0 ]]
      then
        myGroup=2743
      fi
    fi
    
    local groupDir=$BASE_DIR$myGroup
    COMPREPLY=($( compgen -W "$(for x in $groupDir/*; do echo $(basename $x); done)" -- $cur ) )
    return 0
  fi

}



complete -F _set_checked set-checked.sh
