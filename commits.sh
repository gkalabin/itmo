#!/bin/bash

# where is script located
BASE_DIR="$( cd "$( dirname "$0" )" && pwd )" 
# update students repos
WITH_PULL=0
# print only not checked commits
CHECKED_ONLY=1
SHOW_LATEST=0
SHOW_STUDENT_ONLY=""
# format of commit line
GIT_LOG_FORMAT="%h %an %ai >> %s"

# file with checked commits list
CHECKED_COMMITS_FILE=$BASE_DIR/.checked-commits

# count of students with unchecked commits
CNT_UNCHECKED_STUDENTS=0

# group which commits should be displayed
myGROUP=2743

while :
do
  case $1 in
    -g | --group)
      myGROUP=$2
      shift 2
      ;;
    --pull)
      WITH_PULL=1
      shift 1
      ;;
    --all)
      CHECKED_ONLY=0
      shift 1
      ;;
    --one)
      SHOW_LATEST=1
      shift 1
      ;;
    --student)
      SHOW_STUDENT_ONLY=$2
      shift 2 || { echo "wrong"; exit 11; }
      ;;
    --) # End of all options
      shift
      break
      ;;
    *)
      if [ -z $1 ]; then
        # end of params
        break
      fi
      echo "Unrecognized param: $1" >&2
      exit 1
      shift
      ;;
  esac
done

process_dir() {
  # if not dir - skip
  if [ ! -d $1 ]; then
    return 2
  fi
  # if it's not a git repo - skip
  if [[ ! -d $1/.git ]]; then
    return 3
  fi

  # remeber current dir for further return
  local INITIAL_DIR=`pwd`
  # go to dir which should be processed
  cd $1
  # output with student commits
  local OUTPT=""
  # surname of the student (dir base name by convention)
  local STUDENT_NAME=${PWD##*/}

  # go to master branch to prevent teacher fails
  git checkout master 2>&1 >/dev/null 2>&1
  if [ ${WITH_PULL} == 1 ]
  then
    git pull >/dev/null 2>&1
    if [[ $? != 0 ]]; then
      OUTPT=$'\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nCONFLICT\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    fi
  fi

  if [ ${CHECKED_ONLY} == 1 ]
  then
    OIFS="${IFS}"  
    IFS=$'\n'
    # file with stats about changed files
    COMMIT_STATS_FILE=$BASE_DIR/.commits.aux.stats
    # force clean file with file stats
    > $COMMIT_STATS_FILE
    for commit in `git log --format=format:"${GIT_LOG_FORMAT}" algo/` 
    do
      local commit_hash=`echo $commit | cut -f 1 -d" "`
      if ! grep -q "$commit_hash" $CHECKED_COMMITS_FILE 
      then
        # append information about changed files
        git diff-tree --no-commit-id --name-status -r $commit_hash >> $COMMIT_STATS_FILE
        # append commit info
        OUTPT=$OUTPT$'\n'$commit
      fi
    done 
    if [ -s $COMMIT_STATS_FILE ]; then
      COMMIT_STATS_FILE_TMP=$COMMIT_STATS_FILE.tmp
      # sort by second column, so by path, since file stat in the first one
      # for example: M /foo/bar/baz
      # also -u removes duplicates
      sort -k2 -u $COMMIT_STATS_FILE > $COMMIT_STATS_FILE_TMP
      OUTPT=$OUTPT$'\n\nAffected files:\n'`cat $COMMIT_STATS_FILE_TMP`
      # remove unneeded file
      rm $COMMIT_STATS_FILE $COMMIT_STATS_FILE_TMP
    fi
    IFS=$OIFS
  else
    OUTPT=$'\n'`git log --format=format:"${GIT_LOG_FORMAT}" algo/`
  fi
  
  # go back to initial directory
  cd $INITIAL_DIR
  
  if [ ! -z "$OUTPT" ]
  then
    local LAST_CHECK=$2
    hr '=' '='
    printf '%s\n' "$STUDENT_NAME $LAST_CHECK"
    echo -n "==============================="
    echo "$OUTPT"
    printf "\n\n\n"
    CNT_UNCHECKED_STUDENTS=$((CNT_UNCHECKED_STUDENTS+1))  
    return 0
  else
    return 1
  fi
}

cleanup() {  
  # force flag will not produce error when files don't exist
  rm -f .commits.aux*
}


WORKING_DIR=$BASE_DIR/$myGROUP/

# display only requested student
# for example commits.sh --student pupkin --pull
if [[ ! -z $SHOW_STUDENT_ONLY ]]; then
  process_dir $WORKING_DIR/$SHOW_STUDENT_ONLY
  cleanup
  exit 0
fi

hr '#' '#' '#' '#' '#'

# contains collected data about last check time and student name
TMP_FILE1=.commits.aux1
# contains sorted by last check time students
TMP_FILE2=.commits.aux2
cat $CHECKED_COMMITS_FILE | awk '
  {
    if($0 ~ /------/) {
      if(prev ~ /.*\s.*/){
        print prev
      } else {
        print prev, "1970-01-01_00:00"
      }
    }
    prev=$0
  }
' > $TMP_FILE1
sort -k2 $TMP_FILE1 > $TMP_FILE2

while read line; do
  student=`echo $line | cut -f 1 -d" "`
  last_check=`echo $line | cut -f 2 -d" "`
  process_dir $WORKING_DIR/$student $last_check
  if [[ $? == 0 && $SHOW_LATEST == 1 ]]; then
    cleanup
    exit 0
  fi
done < $TMP_FILE2

for dir in $WORKING_DIR/*/
do
  if grep -q `basename $dir` $TMP_FILE2; then
    # already processed
    continue
  fi
  process_dir $dir
  if [[ $? == 0 && $SHOW_LATEST == 1 ]]; then
    cleanup
    exit 0
  fi
done 

cleanup
echo "Unchecked students count: $CNT_UNCHECKED_STUDENTS"
