#!/bin/bash
# where is script located
BASE_DIR="$( cd "$( dirname "$0" )" && pwd )"
# file where checked commits stored
DATA_FILE=$BASE_DIR/.checked-commits
touch $DATA_FILE
# date when check ocurred
NOW=$(date +"%Y-%m-%d_%H:%M")

# format of commit line
GIT_LOG_FORMAT="%h %an %ai >> %s"

GROUP=2743
STUDENT_ONLY=""
COMMITS_ONLY=""
while :
do
  case $1 in
    -g | --group)
      if [[ "2743" != "$2" && "2742" != "$2" ]]
      then
        echo "Unknown group $2"
        exit 1
      fi
      GROUP=$2
      shift 2
      ;;
    -s | --student)
      STUDENT_ONLY=$2
      shift 2
      ;;
    -c | --commit)
      COMMITS_ONLY=$2
      shift 2
      ;;
    --) # End of all options
      shift
      break
      ;;
    *)
      if [ -z $1 ]; then
        # end of params
        break
      fi
      echo "Unrecognized param: $1" >&2
      exit 1
      shift
      ;;
  esac
done

process_dir() {
  CURRENT_DIR=${PWD##*/}
    # if it's not a git repo - skip
  if [[ ! -d .git ]]; then
    echo "ignoring $CURRENT_DIR"
    return 1
  fi
  OIFS="${IFS}"
  IFS=$'\n'
  for commit in `git log --format=format:"$GIT_LOG_FORMAT" algo/` 
  do 
    process_commit $commit
  done
  IFS=$OIFS
}

CURRENT_DIR=""
process_commit() {
  if ! grep -q "$CURRENT_DIR" $DATA_FILE 
  then
    printf "\n\n\n\n${CURRENT_DIR}\n-----------------------------------------------------------\n" >> $DATA_FILE
  fi
    
  
  local commit=$1
  local commit_hash=`echo $commit | cut -f 1 -d" "`

  if ! grep -q "$commit_hash" $DATA_FILE 
  then
    cat $DATA_FILE | awk -v stdnt="$CURRENT_DIR" -v commit="$commit" '
      BEGIN {
        n=-10
        ptrn="^"stdnt
      } 
      {
        if($0 ~ ptrn) {n=NR} 
        if(NR==n+2){print commit; n=-10} 
        print $0
      }
      END {if(n>0) {print commit}}
    ' > $DATA_FILE'temp'
    mv $DATA_FILE'temp' $DATA_FILE
    setup_date $CURRENT_DIR
  fi
}

setup_date() {
  local student=$1
  cat $DATA_FILE | awk -v stdnt="$student" -v now="$NOW" '
      BEGIN {
        ptrn="^"stdnt
      } 
      {
        if($0 ~ ptrn) {
          print stdnt, now
        } else { 
          print $0
        }
      }
    ' > $DATA_FILE'temp'
  mv $DATA_FILE'temp' $DATA_FILE

}

WORKING_DIR=$BASE_DIR/$GROUP
cd $WORKING_DIR

if [ -z $STUDENT_ONLY ] && [ -z "$COMMITS_ONLY" ]
then
  
  echo "Setting all students checked. Type YES to continue"
  read yn
  if [[ $yn != "YES" ]]; then
    echo "Bye"
    exit 0
  fi

  for dir in */
  do
    cd $dir
    process_dir
    cd ..
  done
  exit 0
fi

if [ -d "$STUDENT_ONLY" ] && [ -z "$COMMITS_ONLY" ]
then
  cd $STUDENT_ONLY 
  process_dir 
  cd ..
  exit 0
fi

if [ ! -z "$STUDENT_ONLY" ] && [ ! -z "$COMMITS_ONLY" ]
then
  CURRENT_DIR=$STUDENT_ONLY
  for commit_hash in $COMMITS_ONLY
  do
    process_commit $commit_hash
  done
  exit 0
fi

echo "something wrong"
