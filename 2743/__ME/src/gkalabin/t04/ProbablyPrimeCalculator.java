package gkalabin.t04;

/**
 * @author gkalabin@papeeria.com
 */
public interface ProbablyPrimeCalculator {
  boolean isProbablyPrime(long n);
}
