package gkalabin.t03;

public interface ParadoxModel {

    ProbabilityPair getProbability(int repeats);

    interface ProbabilityPair {
        double getIfChange();
        double getIfNotChange();
    }
}
