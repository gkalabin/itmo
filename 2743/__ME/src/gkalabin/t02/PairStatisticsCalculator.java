package gkalabin.t02;

public interface PairStatisticsCalculator {

    Pair getStatistics(int k);

    interface Pair {
        int getElementIndexFromA();
        int getElementIndexFromB();
        int getSum();
    }

}