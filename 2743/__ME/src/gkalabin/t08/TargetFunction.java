package gkalabin.t08;

import java.util.List;

public interface TargetFunction {

    double getValue(List<TargetResult> entities);

    public interface TargetResult {

        double getValue();

        boolean getResult();
    }
}