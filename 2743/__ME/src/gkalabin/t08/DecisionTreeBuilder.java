package gkalabin.t08;

import java.util.List;

public interface DecisionTreeBuilder<E extends Enum<E>> {

    DecisionTree<E> buildTree(List<DataItem<E>> dataItems, List<DecisionTree.Factor> factors);

    public interface DataItem<E extends Enum<E>> {
        DecisionTree.Entity entity();

        E category();
    }
}