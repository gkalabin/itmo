package gkalabin.t08;

public interface DecisionTree<E extends Enum<E>> {

    E getCategory(Entity entity);

    double getCategoryPrecision(Entity entity, E category);

    public interface Factor {
        String name();

        boolean is(Entity entity);
    }

    public interface Entity {
        String getAttributeValue(String key);
    }

}