package ru.ifmo.enf.trusov.t08;

import ru.ifmo.enf.trusov.t08.learning.Label;
import ru.ifmo.enf.trusov.t08.learning.Preprocessing;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author gkalabin@papeeria.com
 */
public class DTB_trusov {

  private static final File LEARN_FILE = new File("__ME/resources/cs-training.learn.csv");
  private static final File TEST_FILE = new File("__ME/resources/cs-training.test.csv");

  private static DecisionTree<Label> getTree(List<DataItem<Label>> learnItems) throws FileNotFoundException {
    Preprocessing p = new Preprocessing(LEARN_FILE.getPath());
    p.buildData();
    List<DecisionTree.Factor> factors = p.getFactors();
    DecisionTreeBuilderImpl builder = new DecisionTreeBuilderImpl(0.01);
    System.out.println("factors size: "+factors.size());
    return builder.buildTree(learnItems, factors);
  }

  public static void main(String[] args) throws Exception {
    final List<DataItem<Label>> learnItems = loadData(LEARN_FILE);
    System.out.println("learn size: " + learnItems.size());

    final DecisionTree<Label> tree = getTree(learnItems);
    System.out.println("building tree done");

    final List<DataItem<Label>> testItems = loadData(TEST_FILE);
    System.out.println("test size: " + testItems.size());


    final List<TargetFunction.TargetResult> testResults = getResults(tree, testItems);
    final List<TargetFunction.TargetResult> learnResults = getResults(tree, learnItems);

    final TargetFunction auc = new AUC(0.01);
    System.out.println("auc (learn) = " + auc.getValue(learnResults));
    System.out.println("auc (test) = " + auc.getValue(testResults));

  }

  private static List<TargetFunction.TargetResult> getResults(DecisionTree<Label> tree, List<DataItem<Label>> items) {
    List<TargetFunction.TargetResult> result = new ArrayList<>();
    for (DataItem<Label> dataItem : items) {
      result.add(new TargetResultImpl(
          tree.getCategoryPrecision(dataItem.entity(), Label.YES),
          dataItem.category() == Label.YES
      ));
    }
    return result;
  }

  // region DATAload
  private static List<DataItem<Label>> loadData(File file) throws Exception {
    Preprocessing p = new Preprocessing(file.getPath());
    p.buildData();
    return p.getData();
  }
  // endregion


  // region AUC
  static class AUC implements TargetFunction {
    private final double threshold;

    public AUC(final double threshold) {
      this.threshold = threshold;
    }

    public AUC() {
      this.threshold = 0.01;
    }

    @Override
    public double getValue(final List<TargetResult> entities) {

      final List<TargetResult> sortedEntities = new ArrayList<TargetResult>(entities);
      Collections.sort(sortedEntities, new Comparator<TargetResult>() {
        @Override
        public int compare(final TargetResult o1, final TargetResult o2) {
          return new Double(o1.getValue()).compareTo(o2.getValue());
        }
      });

      final Set<DoublePair> points = new HashSet<DoublePair>();
      for (double i = 0.0; i <= 1.0; i += threshold) {
        int tp = 0;
        int tn = 0;
        int fp = 0;
        int fn = 0;
        for (final TargetResult entity : sortedEntities) {
          if (entity.getValue() >= i) {
            if (entity.getResult()) {
              tp++;
            } else {
              fp++;
            }
          } else {
            if (entity.getResult()) {
              fn++;
            } else {
              tn++;
            }
          }
        }
        double se = (double) tp / (tp + fn);
        double sp = (double) tn / (fp + tn);
        points.add(new DoublePair(sp, se));

      }

      final List<DoublePair> pointList = new ArrayList<DoublePair>(points);
      Collections.sort(pointList, new Comparator<DoublePair>() {
        @Override
        public int compare(final DoublePair p1, final DoublePair p2) {
          final int val = new Double(1 - p1.sp).compareTo(1 - p2.sp);
          if (val != 0) {
            return val;
          }
          return new Double(p1.se).compareTo(p2.se);
        }
      });

      double auc = 0;
      for (int i = 0; i < pointList.size() - 1; i++) {
        final DoublePair point1 = pointList.get(i);
        final DoublePair point2 = pointList.get(i + 1);
        final double dx = (1 - point2.sp) - (1 - point1.sp);
        final double dy = point2.se - point1.se;
        auc += dx * point1.se + dx * dy / 2;
      }

      return auc;
    }

    private static class DoublePair {
      private final double se;
      private final double sp;

      private DoublePair(final double sp, final double se) {
        this.sp = sp;
        this.se = se;
      }

      @Override
      public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final DoublePair that = (DoublePair) o;

        if (Double.compare(that.se, se) != 0) return false;
        if (Double.compare(that.sp, sp) != 0) return false;

        return true;
      }

      @Override
      public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(se);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sp);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
      }
    }
  }
  // endregion
}
