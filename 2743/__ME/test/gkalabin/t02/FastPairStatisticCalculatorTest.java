package gkalabin.t02;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FastPairStatisticCalculatorTest {

  private FastPairStatisticsCalculator getCalculator(int[] a, int[] b) {
    return new FastPairStatisticsCalculator(a, b);
  }

  private PairStatisticsCalculator.Pair getStat(FastPairStatisticsCalculator calculator, int zeroBasedIdx) {
    return calculator.getStatistics(zeroBasedIdx);
  }

  @Test
  public void emptyArrays() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{}, new int[]{});
    getStat(calculator, 0);
  }

  @Test
  public void emptyArrayA() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{}, new int[]{1, 2, 3});
    getStat(calculator, 0);
  }

  @Test
  public void emptyArrayB() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 2, 3}, new int[]{});
    getStat(calculator, 0);
  }

  @Test
  public void sameArrays() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{3, 3, 3}, new int[]{2, 2});
    PairStatisticsCalculator.Pair pair = getStat(calculator, 0);
    assertEquals(5, pair.getSum());
  }

  @Test
  public void oneElementArrays() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1}, new int[]{2});
    PairStatisticsCalculator.Pair pair = getStat(calculator, 0);
    assertEquals(3, pair.getSum());
    assertEquals(0, pair.getElementIndexFromA());
    assertEquals(0, pair.getElementIndexFromB());
  }

  @Test
  public void sameTwoElementArrays() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 2}, new int[]{1, 2});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(2, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());
    PairStatisticsCalculator.Pair stat2 = getStat(calculator, 1);
    assertEquals(3, stat2.getSum());
    PairStatisticsCalculator.Pair stat3 = getStat(calculator, 2);
    assertEquals(3, stat3.getSum());
    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(4, stat4.getSum());
    assertEquals(1, stat4.getElementIndexFromA());
    assertEquals(1, stat4.getElementIndexFromB());
  }

  @Test
  public void largeValues() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 1_000_000_000}, new int[]{1, 1_000_000_000});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 3);
    assertEquals(2_000_000_000, stat1.getSum());
    assertEquals(1, stat1.getElementIndexFromA());
    assertEquals(1, stat1.getElementIndexFromB());
  }

  @Test
  public void firstLargerTwoElementArrays() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{10, 20}, new int[]{1, 2});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(11, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat2 = getStat(calculator, 1);
    assertEquals(12, stat2.getSum());
    assertEquals(0, stat2.getElementIndexFromA());
    assertEquals(1, stat2.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat3 = getStat(calculator, 2);
    assertEquals(21, stat3.getSum());
    assertEquals(1, stat3.getElementIndexFromA());
    assertEquals(0, stat3.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(22, stat4.getSum());
    assertEquals(1, stat4.getElementIndexFromA());
    assertEquals(1, stat4.getElementIndexFromB());
  }

  @Test
  public void secondLargerTwoElementArrays() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 2}, new int[]{10, 20});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(11, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat2 = getStat(calculator, 1);
    assertEquals(12, stat2.getSum());
    assertEquals(1, stat2.getElementIndexFromA());
    assertEquals(0, stat2.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat3 = getStat(calculator, 2);
    assertEquals(21, stat3.getSum());
    assertEquals(0, stat3.getElementIndexFromA());
    assertEquals(1, stat3.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(22, stat4.getSum());
    assertEquals(1, stat4.getElementIndexFromA());
    assertEquals(1, stat4.getElementIndexFromB());
  }

  @Test
  public void firstLargerDifferLength() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{10, 20, 30}, new int[]{1, 2});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(11, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat2 = getStat(calculator, 1);
    assertEquals(12, stat2.getSum());
    assertEquals(0, stat2.getElementIndexFromA());
    assertEquals(1, stat2.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat3 = getStat(calculator, 2);
    assertEquals(21, stat3.getSum());
    assertEquals(1, stat3.getElementIndexFromA());
    assertEquals(0, stat3.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(22, stat4.getSum());
    assertEquals(1, stat4.getElementIndexFromA());
    assertEquals(1, stat4.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat5 = getStat(calculator, 4);
    assertEquals(31, stat5.getSum());
    assertEquals(2, stat5.getElementIndexFromA());
    assertEquals(0, stat5.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat6 = getStat(calculator, 5);
    assertEquals(32, stat6.getSum());
    assertEquals(2, stat6.getElementIndexFromA());
    assertEquals(1, stat6.getElementIndexFromB());
  }

  @Test
  public void secondLargerDifferLength() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 2}, new int[]{10, 20, 30});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(11, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat2 = getStat(calculator, 1);
    assertEquals(12, stat2.getSum());
    assertEquals(1, stat2.getElementIndexFromA());
    assertEquals(0, stat2.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat3 = getStat(calculator, 2);
    assertEquals(21, stat3.getSum());
    assertEquals(0, stat3.getElementIndexFromA());
    assertEquals(1, stat3.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(22, stat4.getSum());
    assertEquals(1, stat4.getElementIndexFromA());
    assertEquals(1, stat4.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat5 = getStat(calculator, 4);
    assertEquals(31, stat5.getSum());
    assertEquals(0, stat5.getElementIndexFromA());
    assertEquals(2, stat5.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat6 = getStat(calculator, 5);
    assertEquals(32, stat6.getSum());
    assertEquals(1, stat6.getElementIndexFromA());
    assertEquals(2, stat6.getElementIndexFromB());
  }

  @Test
  public void sameDifferLength() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 2}, new int[]{1, 2, 3});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(2, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat2 = getStat(calculator, 1);
    assertEquals(3, stat2.getSum());

    PairStatisticsCalculator.Pair stat3 = getStat(calculator, 2);
    assertEquals(3, stat3.getSum());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(4, stat4.getSum());

    PairStatisticsCalculator.Pair stat5 = getStat(calculator, 4);
    assertEquals(4, stat5.getSum());

    PairStatisticsCalculator.Pair stat6 = getStat(calculator, 5);
    assertEquals(5, stat6.getSum());
    assertEquals(1, stat6.getElementIndexFromA());
    assertEquals(2, stat6.getElementIndexFromB());
  }

  @Test
  public void threeItemsSame() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 2, 3}, new int[]{1, 2, 3});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(2, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(4, stat4.getSum());

    PairStatisticsCalculator.Pair stat9 = getStat(calculator, 8);
    assertEquals(6, stat9.getSum());
    assertEquals(2, stat9.getElementIndexFromA());
    assertEquals(2, stat9.getElementIndexFromB());
  }

  @Test
  public void threeItemsFirstLarger() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{10, 20, 30}, new int[]{1, 2, 3});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(11, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(21, stat4.getSum());
    assertEquals(1, stat4.getElementIndexFromA());
    assertEquals(0, stat4.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat9 = getStat(calculator, 8);
    assertEquals(33, stat9.getSum());
    assertEquals(2, stat9.getElementIndexFromA());
    assertEquals(2, stat9.getElementIndexFromB());
  }

  @Test
  public void threeItemsSecondLarger() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1, 2, 3}, new int[]{10, 20, 30});
    PairStatisticsCalculator.Pair stat1 = getStat(calculator, 0);
    assertEquals(11, stat1.getSum());
    assertEquals(0, stat1.getElementIndexFromA());
    assertEquals(0, stat1.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat4 = getStat(calculator, 3);
    assertEquals(21, stat4.getSum());
    assertEquals(0, stat4.getElementIndexFromA());
    assertEquals(1, stat4.getElementIndexFromB());

    PairStatisticsCalculator.Pair stat9 = getStat(calculator, 8);
    assertEquals(33, stat9.getSum());
    assertEquals(2, stat9.getElementIndexFromA());
    assertEquals(2, stat9.getElementIndexFromB());
  }

  @Test
  public void wrongIndex() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{1}, new int[]{2});
    fail("Got: " + getStat(calculator, 2));
  }

  @Test
  public void unsortedArray() {
    FastPairStatisticsCalculator calculator = getCalculator(new int[]{2, 1}, new int[]{2});
    fail("Got: " + getStat(calculator, 0));
  }
}
