package gkalabin.t04;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class PrimalityTestImplTest {
  private PrimalityTestImpl getCalculator() {
    return new PrimalityTestImpl(new Random(100500L));
  }

  @Test
  public void testPrimeUpTo10() {
    ProbablyPrimeCalculator probablyPrimeCalculator = getCalculator();
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(2));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(3));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(5));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(7));
  }

  @Test
  public void testNotPrimeUpTo10() {
    ProbablyPrimeCalculator probablyPrimeCalculator = getCalculator();
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(4));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(6));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(8));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(9));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(10));
  }

  @Test
  public void testPrime() {
    ProbablyPrimeCalculator probablyPrimeCalculator = getCalculator();
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(11));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(13));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(17));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(19));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(23));
    Assert.assertTrue(probablyPrimeCalculator.isProbablyPrime(29));
  }

  @Test
  public void testNotPrime() {
    ProbablyPrimeCalculator probablyPrimeCalculator = getCalculator();
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(12));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(14));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(15));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(16));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(18));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(20));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(21));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(22));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(24));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(25));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(26));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(27));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(28));
    Assert.assertFalse(probablyPrimeCalculator.isProbablyPrime(30));
  }
}
