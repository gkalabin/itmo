package gkalabin.t03;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParadoxModelImplTest {
  private ParadoxModelImpl getModel() {
    return new ParadoxModelImpl();
  }

  @Test
  public void probabilities() {
    ParadoxModel model = getModel();
    ParadoxModel.ProbabilityPair pair = model.getProbability(10_000);
    assertEquals(0.33d, pair.getIfNotChange(), 0.05d);
    assertEquals(0.66d, pair.getIfChange(), 0.05d);
  }

  @Test
  public void sameResults() {
    ParadoxModel model1 = getModel();
    ParadoxModel model2 = getModel();
    ParadoxModel.ProbabilityPair pair1 = model1.getProbability(100);
    ParadoxModel.ProbabilityPair pair2 = model2.getProbability(100);
    assertEquals(pair1.getIfChange(), pair2.getIfChange(), 0.0001d);
    assertEquals(pair1.getIfNotChange(), pair2.getIfNotChange(), 0.0001d);
  }
}
