package gkalabin.t08;

import com.google.common.collect.Maps;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * @author gkalabin@papeeria.com
 */
public class DTB {

  private static final File LEARN_FILE = new File("__ME/resources/cs-training.learn.csv");
  private static final File TEST_FILE = new File("__ME/resources/cs-training.test.csv");

  private static DecisionTree<YesNo> getTree(List<DecisionTreeBuilder.DataItem<YesNo>> learnItems) {
    return null;
  }
  
  public static void main(String[] args) throws Exception {
    System.out.println(new File(".").getCanonicalPath());
    final List<DecisionTreeBuilder.DataItem<YesNo>> learnItems = loadData(LEARN_FILE);
    System.out.println("learn size: " + learnItems.size());

    final DecisionTree<YesNo> tree = getTree(learnItems);
    System.out.println("building tree done");

    final List<DecisionTreeBuilder.DataItem<YesNo>> testItems = loadData(TEST_FILE);
    System.out.println("test size: " + testItems.size());


    final List<TargetFunction.TargetResult> testResults = new ArrayList<>();
    for (DecisionTreeBuilder.DataItem<YesNo> dataItem : testItems) {
      testResults.add(new TargetResultImpl(
          tree.getCategoryPrecision(dataItem.entity(), YesNo.YES),
          dataItem.category() == YesNo.YES
      ));
    }

    final List<TargetFunction.TargetResult> learnResults = new ArrayList<>();
    for (DecisionTreeBuilder.DataItem<YesNo> dataItem : learnItems) {
      learnResults.add(new TargetResultImpl(
          tree.getCategoryPrecision(dataItem.entity(), YesNo.YES),
          dataItem.category() == YesNo.YES
      ));
    }

    final TargetFunction auc = new AUC(0.01);
    System.out.println("auc (learn) = " + auc.getValue(learnResults));
    System.out.println("auc (test) = " + auc.getValue(testResults));

  }

  private static enum YesNo {
    YES, NO
  }

  // region DATAload
  private static List<DecisionTreeBuilder.DataItem<YesNo>> loadData(File file) throws Exception {
    final Scanner scanner = new Scanner(file);
    final List<DecisionTreeBuilder.DataItem<YesNo>> items = new ArrayList<>();

    boolean first = true;
    while (scanner.hasNext()) {
      final String line = scanner.nextLine();
      if (first) {
        first = false;
        continue;
      }
      final String[] splitted = line.split(",");
      final EntityImpl entity = new EntityImpl();
      entity.addAttribute("id", splitted[0]);
      entity.addAttribute("RevolvingUtilizationOfUnsecuredLines", splitted[2]);
      entity.addAttribute("age", splitted[3]);
      entity.addAttribute("NumberOfTime30-59DaysPastDueNotWorse", splitted[4]);
      entity.addAttribute("DebtRatio", splitted[5]);
      entity.addAttribute("MonthlyIncome", splitted[6]);
      entity.addAttribute("NumberOfOpenCreditLinesAndLoans", splitted[7]);
      entity.addAttribute("NumberOfTimes90DaysLate", splitted[8]);
      entity.addAttribute("NumberRealEstateLoansOrLines", splitted[9]);
      entity.addAttribute("NumberOfTime60-89DaysPastDueNotWorse", splitted[10]);
      entity.addAttribute("NumberOfDependents", splitted[11]);

      final YesNo result = Integer.parseInt(splitted[1]) == 1 ? YesNo.YES : YesNo.NO;
      items.add(new DataItemImpl<>(entity, result));
    }
    return items;
  }
  // endregion

  
  // region AUC
  static class AUC implements TargetFunction {
    private final double threshold;

    public AUC(final double threshold) {
      this.threshold = threshold;
    }

    public AUC() {
      this.threshold = 0.01;
    }

    @Override
    public double getValue(final List<TargetResult> entities) {

      final List<TargetResult> sortedEntities = new ArrayList<TargetResult>(entities);
      Collections.sort(sortedEntities, new Comparator<TargetResult>() {
        @Override
        public int compare(final TargetResult o1, final TargetResult o2) {
          return new Double(o1.getValue()).compareTo(o2.getValue());
        }
      });

      final Set<DoublePair> points = new HashSet<DoublePair>();
      for (double i = 0.0; i <= 1.0; i += threshold) {
        int tp = 0;
        int tn = 0;
        int fp = 0;
        int fn = 0;
        for (final TargetResult entity : sortedEntities) {
          if (entity.getValue() >= i) {
            if (entity.getResult()) {
              tp++;
            } else {
              fp++;
            }
          } else {
            if (entity.getResult()) {
              fn++;
            } else {
              tn++;
            }
          }
        }
        double se = (double) tp / (tp + fn);
        double sp = (double) tn / (fp + tn);
        points.add(new DoublePair(sp, se));

      }

      final List<DoublePair> pointList = new ArrayList<DoublePair>(points);
      Collections.sort(pointList, new Comparator<DoublePair>() {
        @Override
        public int compare(final DoublePair p1, final DoublePair p2) {
          final int val = new Double(1 - p1.sp).compareTo(1 - p2.sp);
          if (val != 0) {
            return val;
          }
          return new Double(p1.se).compareTo(p2.se);
        }
      });

      double auc = 0;
      for (int i = 0; i < pointList.size() - 1; i++) {
        final DoublePair point1 = pointList.get(i);
        final DoublePair point2 = pointList.get(i + 1);
        final double dx = (1 - point2.sp) - (1 - point1.sp);
        final double dy = point2.se - point1.se;
        auc += dx * point1.se + dx * dy / 2;
      }

      return auc;
    }

    private static class DoublePair {
      private final double se;
      private final double sp;

      private DoublePair(final double sp, final double se) {
        this.sp = sp;
        this.se = se;
      }

      @Override
      public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final DoublePair that = (DoublePair) o;

        if (Double.compare(that.se, se) != 0) return false;
        if (Double.compare(that.sp, sp) != 0) return false;

        return true;
      }

      @Override
      public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(se);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sp);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
      }
    }
  }
  // endregion
  
  // region crap
  static class DataItemImpl<E extends Enum<E>> implements DecisionTreeBuilder.DataItem<E> {
    private final DecisionTree.Entity myEntity;
    private final E myCategory;

    public DataItemImpl(DecisionTree.Entity entity, E category) {
      myEntity = entity;
      myCategory = category;
    }

    @Override
    public DecisionTree.Entity entity() {
      return myEntity;
    }

    @Override
    public E category() {
      return myCategory;
    }
  }

  static class EntityImpl implements DecisionTree.Entity {
    private final Map<String, String> myEntity = Maps.newHashMap();

    @Override
    public String getAttributeValue(String key) {
      return myEntity.get(key);
    }

    public void addAttribute(String key, String value) {
      myEntity.put(key, value);
    }
  }

  static class TargetResultImpl implements TargetFunction.TargetResult {
    private final double myValue;
    private final boolean myResult;

    public TargetResultImpl(double value, boolean result) {
      myValue = value;
      myResult = result;
    }

    @Override
    public double getValue() {
      return myValue;
    }

    @Override
    public boolean getResult() {
      return myResult;
    }
  }
  // endregion
}
