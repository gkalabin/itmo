package gkalabin.t01;


import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

public class RandomizedStatisticsCalculatorTest {
  private StatisticsCalculator<Integer> getCalculator(Integer[] data) {
    return new RandomizedStatisticsCalculator<>(data, 10L);
  }

  private Integer getStatistics(StatisticsCalculator<Integer> calculator, int idx) {
    return calculator.getStatistics(idx);
  }

  @Test
  public void unsortedArray() {
    for (int i = 0; i < 20; i++) {
      Integer[] data = new Integer[]{6, 4, 3, 5, 7, 9};
      StatisticsCalculator<Integer> calculator = getCalculator(data);
      assertEquals((Integer) 5, getStatistics(calculator, 2));
    }
  }

  @Test
  public void oneElementInput() {
    for (int i = 0; i < 20; i++) {
      Integer[] data = new Integer[]{1};
      StatisticsCalculator<Integer> calculator = getCalculator(data);
      assertEquals((Integer) 1, getStatistics(calculator, 0));
    }
  }

  @Test
  public void twoItems() {
    for (int i = 0; i < 20; i++) {
      Integer[] data = new Integer[]{77, 99};
      StatisticsCalculator<Integer> calculator = getCalculator(data);
      assertEquals((Integer) 77, getStatistics(calculator, 0));
      assertEquals((Integer) 99, getStatistics(calculator, 1));
    }
  }

  @Test
  public void threeItems() {
    for (int i = 0; i < 20; i++) {
      Integer[] data = new Integer[]{77, 99, 131};
      StatisticsCalculator<Integer> calculator = getCalculator(data);
      assertEquals((Integer) 77, getStatistics(calculator, 0));
      assertEquals((Integer) 99, getStatistics(calculator, 1));
      assertEquals((Integer) 131, getStatistics(calculator, 2));
    }
  }

  @Test
  public void twoItemsReversed() {
    for (int i = 0; i < 20; i++) {
      Integer[] data = new Integer[]{99, 77};
      StatisticsCalculator<Integer> calculator = getCalculator(data);
      assertEquals((Integer) 77, getStatistics(calculator, 0));
      assertEquals((Integer) 99, getStatistics(calculator, 1));
    }
  }

  @Test
  public void threeItemsReversed() {
    for (int i = 0; i < 20; i++) {
      Integer[] data = new Integer[]{131, 99, 77};
      StatisticsCalculator<Integer> calculator = getCalculator(data);
      assertEquals((Integer) 77, getStatistics(calculator, 0));
      assertEquals((Integer) 99, getStatistics(calculator, 1));
      assertEquals((Integer) 131, getStatistics(calculator, 2));
    }
  }

  @Test
  public void saveSourceData() {
    for (int i = 0; i < 20; i++) {
      Integer[] data = new Integer[]{6, 4, 3, 5, 7, 9};
      Integer[] copy = data.clone();
      StatisticsCalculator<Integer> calculator = getCalculator(data);
      getStatistics(calculator, 2);
      assertArrayEquals(copy, data);
    }
  }

  @Test
  public void idxOutLeft() {
    getCalculator(new Integer[]{6, 4, 3, 5, 7, 9}).getStatistics(-2);
    fail("wtf?");
  }

  @Test
  public void idxOutRight() {
    getCalculator(new Integer[]{6, 4, 3, 5, 7, 9}).getStatistics(77);
    fail("wtf?");
  }
}

