#!/bin/bash

# where is script located
BASE_DIR="$( cd "$( dirname "$0" )" && pwd )" 

# group which commits should be displayed
myGROUP=2743

STUDENTS=""
TASKS=""

while :
do
  case $1 in
    -g | --group)
      myGROUP=$2
      shift 2
      ;;
    --tasks)
      TASKS=$2
      shift 2 || { echo "wrong"; exit 11; }
      ;;
    --students)
      STUDENTS=$2
      shift 2 || { echo "wrong"; exit 11; }
      ;;
    --) # End of all options
      shift
      break
      ;;
    *)
      if [ -z $1 ]; then
        # end of params
        break
      fi
      echo "Unrecognized param: $1" >&2
      exit 1
      shift
      ;;
  esac
done

# $1 - student 
# $2 - tasks
process_student() {
  # if not dir - skip
  if [ ! -d $1 ]; then
    echo "!!! $1 not found"
    return 2
  fi

  # remeber current dir for further return
  local INITIAL_DIR=`pwd`
  # go to dir which should be processed
  cd $1
  # surname of the student (dir base name by convention)
  local STUDENT_NAME=${PWD##*/}
  local tasks=$2

  hr '=' '='
  printf '%s\n' "$STUDENT_NAME"
  echo "==============================="
  for task in $tasks; do
    task_path="algo/src/java/ru/ifmo/enf/$STUDENT_NAME/t$task/"
    if [[ ! -d $task_path ]]; then
        echo "Task #$task - NONE"
    else
      echo "Task #"$task
      OUTPT=$'\n'`git log --format=format:"%h %an %ai >> %s" $task_path`
      echo "$OUTPT"
      printf "\n\n"
    fi
  done
  printf "\n\n\n"
  cd $INITIAL_DIR
}


WORKING_DIR=$BASE_DIR/$myGROUP/
cd $WORKING_DIR

hr '#' '#' '#' '#' '#'

if [[ ! -z $STUDENTS ]]; then
  for student in $STUDENTS; do
    process_student $student "$TASKS"
  done
else
  # process all students if explicitly not specified
  for dir in $WORKING_DIR/*/; do
    REPO=$dir/.git/
    if [[ -d $REPO ]]; then
      process_student $dir "$TASKS"
    fi
  done
fi
